// Shared code needed by the code of all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.eng1003.fencingApp";

// Array of saved Region objects.
var savedRegions = [];

class Region {
    constructor() {
        this.cornerLocations = [];
        this.date;
    }
    setCorners(coordinates) {
        this.cornerLocations = coordinates;
    }
    addCorner(coordinate) {
        this.cornerLocations.push(coordinate);
    }
    setDate(date) {
        this.date = date;
    }
}


class RegionList {
    constructor(regions, date) {
        this.regions = regions;
        this.numberOfRegions = numberOfRegions;
    }
    add() { }
    get() { }
    remove() { }
}
